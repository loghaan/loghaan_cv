.. |pagebreak| raw:: html

  <div class="pagebreak"></div>

.. |empty| raw:: html

  <div></div>

.. |br| raw:: html

  <br/>

.. |rsquo| raw:: html

  &rsquo;

===========
 Sébastien
===========
------
 MATZ
------

.. container:: title

  Ingénieur Logiciel Linux

  *20 ans d’expérience*


First page left zone
********************

Contact
=======

  🖄 loghaan@gmail.com

  🕸 https://gitlab.com/loghaan

  🕻 +33 6 63 03 07 29

  🖆 20 lieu dit le Brossais |br|
  35230 BOURGBARRE


Formation
=========

2003
~~~~

Diplôme d'ingénieur IFSIC à Rennes *(Informatique et Architecture des Machines)*


2001
~~~~

Licence/Maitrise ISTIA à Angers *(Automatique et Informatique Industrielle)*


1999
~~~~

DUT Électronique et Informatique Industrielle à Ville d'Avray *(région parisienne)*

Permis B
~~~~~~~~

|empty|


Expériences (en bref)
=====================

**2016-2023** Ingénieur logiciel embarqué Linux à `Veo-Labs`_

**2015** Ingénieur logiciel réseau en prestation chez `Canon-CRF`_

**2014-2015** Ingénieur logiciel embarqué en prestation chez `Faiveley`_

**2007-2013** Ingénieur Logiciel compilation à `CAPS-Entreprise`_

**2006-2007** Ingénieur Expert compilation à l |rsquo| `IRISA RENNES`_

First page right zone
*********************

Compétences
===========

Fonctionnel
~~~~~~~~~~~

*Design* et élaboration de logiciels Linux (spécification, développement,
déploiement, tests et *packaging* de l'application)

Documentation technique et manuel utilisateur (anglais et français)

Responsable technique, *Lead developer* et encadrement d’ingénieurs


Systèmes et Bootloaders
~~~~~~~~~~~~~~~~~~~~~~~

``UNIX`` en général et ``Linux`` en particulier (*desktop*, administration système,
embarqué, *drivers* et *kernel*), ``U-Boot``


Langages
~~~~~~~~

``C, C++, python, SQL, HTML, CSS, Javascript, bash``


Méthodologie
~~~~~~~~~~~~

*Test Driven Development*, ``Scrum, UML, EBNF Grammars``


Processeurs et Boards
~~~~~~~~~~~~~~~~~~~~~

``ARM, AVR, Rock64, RaspberryPi``


Outils de développement
~~~~~~~~~~~~~~~~~~~~~~~

``gcc, make, SConstruct, gdb, buildroot, yocto, swig, pytest, git, Subversion``

Protocoles et Normes
~~~~~~~~~~~~~~~~~~~~

``MIDI, Ethernet, IPv4, ARP, UDP, TCP, HTTP, SMTP, POP3,
DHCP, DNS``


Composants et Frameworks
~~~~~~~~~~~~~~~~~~~~~~~~

``python3/asyncio, OpenGL, ALSA, LLVM/Clang, LibFFI,
sqlite3, pthread, boost, glib, Qt4, gtk3``


Autres logiciels
~~~~~~~~~~~~~~~~

``docker, jenkins, qemu, ssh, scapy, wireshark, sphinx, plantuml, weasyprint, Kicad, Eagle``


Langues
~~~~~~~

**Français** *(langue maternelle)*, **Anglais** *(niveau B2)*, **Allemand** *(niveau scolaire)*



Next pages
**********

|pagebreak|


Expériences (en détails)
========================

Veo-Labs
~~~~~~~~

.. container:: info

    2016-2023
      Ingénieur Logiciel **Linux embarqué** pendant **7 ans**
      au sein de l'équipe "Veobox"* à **Rennes**

L’équipe "Veobox" de Veo-Labs, composée de 3 à 5 ingénieurs, développe deux produits
de type *set-top box*: la Veobox3_ et la VeoboxM_. L’objectif de ces deux produits est
d’enregistrer et de retransmettre de la vidéo dans le domaine de la formation, de la santé
et de la communication d’entreprise. Ces deux produits proposent une interface *web* pour
leur utilisation. La Veobox3_ est utilisée en production et reconnue comme stable. Elle
est destinée à travailler avec des *devices* audio-visuelles tiers compatibles HDMI, SDI ou VGA.
La VeoboxM_ est un nouveau produit. Elle est destinée à travailler avec des *devices* audio-visuelles
fonctionnant directement en IP et a été lancée en 2021. 


VeoboxM
-------

La VeoboxM_ est un système embarqué ``linux`` basé sur une plateforme ``ARM RK3328``.
Voici les différentes tâches qui ont été effectuées sur ce projet:

  - *Design* de l’architecture logicielle du système embarqué
  - Génération de la distribution ``linux`` avec ``yocto``
  - Écriture des *scripts* de *flashing*
  - Prototype d’un module ``kernel iptables`` pour dupliquer et transmettre des flux UDPTS localement
    à différents *process*
  - État des lieux des performances du système embarqué (Réseau, *storage*, ...)
  - Développement et maintenance de l'application "métier" côté *backend* en ``python3/asyncio``
    (contrôle d’instances de ``ffmpeg``, *webservices* en ``websockets``, ...)
  - Prototype de l'interface *web* en attendant l'arrivée d'un vrai développeur *front*
    en ``HTML/CSS/vanilla Javascript``
  - Quelques ajouts *ad hoc* au programme ``ffmpeg``
  - Mise en place de l'environnement de dev. en dehors ``yocto`` avec montage ``sshfs`` et possibilité 
    de lancer une grande partie de l'application métier sur le PC du développeur
  - Mise en place du *framework* de test basé sur ``pytest`` (contrôle du *shell* via la liaison série/SSH,
    contrôle de l'alimentation électrique, *flashing* depuis le test auto, ...)
  - Développement spécifique dans U-Boot (*factory reset*, *multi-banking*)
  - Intégration continue basé sur ``jenkins`` / ``docker`` / ``pytest``
  - Prototype de l’écran de *monitoring* (sortie HDMI du produit) basé sur ``weston`` / ``OpenGL`` / SDL2 / ``python3``
  - Rédaction de divers documentations (cahier des charges, documentation technique, manuel utilisateur)
  - Autres travaux annexes de maintenance, de *debugging*, ajout de *features*, intégration ``yocto``,
    gestion de configuration avec ``git``, électronique générale et programmation *web*.
  - Écriture de tests automatiques (fonctionnel, performances, endurance, ...)
  - Avant-vente et support


.. container:: skills

   Environnement UNIX, kernel linux, C, Javascript, HTML, CSS, python3, asyncio, git, SConstruct,
   restructuredText, sphinx, pytest, yocto, rockchip, ffmpeg, docker, RaspberryPi, électronique


Veobox3
-------

La Veobox3_ est un système  embarqué ``linux`` basé sur une plateforme ``ARM imx6``. Ci-dessous les différentes
tâches qui ont été effectuées sur ce produit:

  - Mise en place d’une procédure de tests pour vérifier le *hardware* de la Veobox3_ en "sortie d’usine".
  - La solution déployée est basée sur le *framework* de test python2 ``nosetests``.
  - Développement d’un élément ``gstreamer`` permettant de composer plusieurs sources vidéo en une seule. Le
    composant ``gstreamer`` développé utilise l’accélération *hardware* via l'API ``video4linux2/m2m``
  - Mise en place de l'environnement de dev. en dehors de ``yocto`` avec *remote debugging*, *cross building*,
    montage ``NFS`` et possibilité de lancer le *pipeline* ``gstreamer`` sur la machine du développeur
  - Modification d'un module ``kernel`` ``video4linux2/m2m`` pour simuler le *hardware* de la *target*
  - Mise en place d’un système de *profiling custom* pour *tracker* les derniers *bugs* du composant
    ``gstreamer`` de composition
  - Développement d’un vu-mètre audio utilisé en *preview* et pendant l’enregistrement (élément ``gstreamer``,
    ``framebuffer linux``).
  - Optimisation de l’algorithme de reconnaissance de *slides* (détection d’une image fixe dans une vidéo) en
    utilisant le jeu d’instruction ``ARM/Neon`` (élément ``gstreamer``)
  - Autres travaux annexes de maintenance, de *debugging*, ajout de *features*, intégration ``yocto``,
    gestion de configuration avec ``git``, électronique générale et programmation *web*.
  - Avant-vente et support

.. container:: skills

  Environnement UNIX, kernel linux, C, python2, HTML, CSS, Javascript, gdb, git, SConstruct, nosetests, yocto,
  imx6, gstreamer, restructuredText, électronique


Canon-CRF
~~~~~~~~~

  .. container:: info

    2015
      Mission de **7 mois** en tant que prestataire Eurogiciel |br|
      Ingénieur Logiciel **Linux réseau** |br|
      poste basé à **Rennes**

Au sein d'une petite équipe de 2 ingénieurs, implémentation d'un protocole réseau propriétaire
dans le noyau ``linux`` situé entre la couche ``Ethernet`` et la couche ``IPv4``. Elaboré
par les chercheurs de Canon-CRF_ il permet de prendre en compte et d'exploiter les
caractéristiques du ``wifi`` à  60GHz. Le protocole imaginé par l'équipe de
recherche offre les *features* suivantes:

  - Le transit de paquets à travers plusieurs routes au sein d'un réseau maillé
  - Un système de routage et de *forwarding*
  - La reconfiguration dynamique des routes
  - La reconstruction des paquets corrompus

Détail des tâches effectuées pendant la mission:

  - Documentation (spécification technique du protocole, architecture du
    module et de son utilisation au niveau applicatif)
  - Mise en place du système de gestion de configuration basé sur ``git``
  - Mise en place de l'environnement de développement basé sur ``qemu``
    et permettant de simuler un réseau entier comprenant plusieurs machines
    virtuelles de type ``linux/x86``. Le ``kernel`` de chaque VM peut
    être *debuggée* avec ``gdb``.
  - Développement en ``C`` de différents composants logiciels (abstraction de l'OS,
    injection des paquets avec ``iptables``, module de transmission et de réception)
  - Développement de l'application ``C/C++`` permettant de paramétrer le module via
    ``/proc`` et ``/sys``, de faire du *monitoring* et de reconfigurer les routes
    (utilisation de ``MetaStateMachine`` de la *library* ``boost``)
  - Mise en place du *framework* de test basé sur ``unittest``, ``python2/twisted``,
    ``pyserial``, et ``scapy``.
  - Écriture de tests automatiques (*stimuli* sur le réseau virtuel et sur les *shells* des VMs,
    simulation d'erreurs de transmission)
  - Génération automatique d'un rapport d'exécution des tests

.. container:: skills

    Environnement UNIX, kernel linux, C, C++, python2, gdb, scapy, wireshark, boost, qemu,
    Ethernet, IPv4, UDP, iptables, git, SConstruct, restructuredText, Test Driven Development


Faiveley
~~~~~~~~

.. container:: info

  2014-2015
    Mission de **9 mois** en tant que prestataire Eurogiciel |br|
    Ingénieur Logiciel **Linux embarqué** |br|
    poste basé la majeur partie du temps à **Rennes** +quelques jours chez le client à **Tours** |br|
    Forfait

Dans le cadre d’un projet forfaitaire pour l’industrie ferroviaire:

  - Intégration d’une distribution ``linux`` temps-réel embarqué pour une architecture basée
    sur un processeur ``ColdFire`` (famille ``Motorola 68k``)
  - Développement de deux *drivers* ``linux`` (*driver* ``GPIO``, ``DAC`` pour génération
    de signaux sonores compatible ``OSS /dev/dsp``)
  - Développements dans ``U-Boot`` (*flashing*, contrôle des ``LEDs`` de la *board*)
  - Mise en place du *framework* de tests (basé sur ``unittest``) permettant la
    prise de contrôle du *shell* de la *board* (``U-Boot`` et ``linux``) via la
    liaison série. Contrôle de l'alimentation de la *board*
  - Écriture de tests automatiques (fonctionnel, performances, ...)
  - Génération automatique (à partir des tests automatiques) d'un cahier de test pour effectuer
    la recette chez le client
  - Évaluation et sélection des systèmes de fichiers
  - Intégration de divers *packages* dans ``buildroot``

.. container:: skills

     Environnement UNIX, kernel linux, C, python2, restructuredText, buildroot, |br|
     U-Boot, IPv4, iperf, électronique


CAPS-Entreprise
~~~~~~~~~~~~~~~

.. container:: info

  2007-2013
    Ingénieur Logiciel **Compilation** au sein de l'équipe "produit" (3 à 6 ingénieurs) |br|
    durée de **6 ans** |br|
    poste basé à **Rennes**

Participation au développement de deux produits: CapsCF_ et HMPP_
dans le domaine de l'optimisation de code, de la compilation et de l'High
Performance Computing.


CapsCF
------

CapsCF_ est un outil pour isoler des routines et le jeu de données associés
provenant d'une application écrite en ``C`` ou en ``Fortran``. Cet outil a été
commandé par un labo de recherche (Université de Versailles). Voici les différentes
tâches qui ont pu être effectués sur ce logiciel:

  - Élaboration du prototype
  - *Lead Developer* sur le projet (encadrement de 2 ingénieurs)
  - *Design* complet du logiciel
  - Développement en ``C++`` des différents composants logiciel
  - Système pour enregistrer, modifier, *sérialiser* et rejouer le processus de *build*
  - Intégration automatique de différents *profilers*  (``gprof``, ``scalasca``,  ``profile-loops``, ...)
    pour trouver les différents *hotspots*
  - Transformation des *hotspots* de l'application en fonction isolée
  - Instrumentation de l'application pour pouvoir extraire le jeu de donnée utilisée par les *hotspots*
  - Système pour pouvoir rejouer en dehors de l'application la fonction extraite
    avec son jeu de données
  - Développement de l'infrastructure de test basé sur ``unittest``
  - *packaging* de l'application
  - Participation aux réunions hebdomadaires avec le client en tant que
    responsable technique
  - Rédaction de documents techniques à usage interne (en français)
  - Rédaction du manuel utilisateur du produit (en anglais)
  - Étude pour une évolution du logiciel basée sur le *framework*
    de compilation *open source* ``LLVM`` / ``Clang`` permettant la 
    prise en charge des applications écrites en ``C++``

.. container:: skills

  Environnement UNIX, Compilation, EBNF Grammars, C, C++, python2, gdb, x86,
  swig, HTML, SQL, Fortran, OpenMP, MPI, LibFFI, LLVM/Clang, Subversion,
  SConstruct, restructuredText, Apache, MySQL, PhpMyAdmin, gcc, icc, gprof,
  Scrum, Conduite de projet, UML


HMPP
----

HMPP_ est un compilateur optimisant qui permet de déporter du code C ou Fortran
sur un GPU à l'aide de directives (type ``#pragma``). Voici les différentes tâches
effectuées sur ce logiciel:

  - *Design* de l'arbre syntaxique (``AST``) et du langage intermédiaire utilisés
    en interne par le compilateur
  - Élaboration de la *library* de *runtime* et du format des exécutables générés
    par le compilateur
  - Développement logiciel dans le compilateur en ``C++`` (algo d'optimisation dans l |rsquo| ``AST``,
    *backend* ``cuda`` et ``OpenCL``, *parsing* du langage intermédiaire)
  - Système de *build* basé sur ``SConstruct``
  - Développement de *bindings* ``python2``
  - Développement de l'infrastructure de test en ``python2/unittest`` permettant:

    - l'exécution d'une transformation de code spécifique    
    - la comparaison de deux arbres syntaxiques
    - la génération automatique d'un jeu de donnée à partir d'un prototype de fonction
    - la comparaison de deux jeux de données résultant du même code compilé par le compilateur
      de référence et par HMPP_
    - de *plugger* automatiquement ``gdb`` sur les tests
  
  - Rédaction de documents techniques à usage interne (en français)

.. container:: skills

  Environnement UNIX, Compilation, EBNF Grammars, C, C++, python2, gdb, swig, HTML, XML, SQL,
  Fortran, LibFFI, cuda, OpenCL, OpenMP, MPI, Subversion, SConstruct, restructuredText, Apache,
  MySQL, PhpMyAdmin, Scrum, UML, Test Driven Development



IRISA Rennes
~~~~~~~~~~~~

.. container:: info

  2006-2007
    **18 mois** en tant qu'ingénieur expert en labo de recherche dans le domaine du **GPGPU**
    et de la **compilation**

Développement d'un compilateur expérimental permettant de transformer une routine écrite
en ``C`` vers du code  s'exécutant sur des ``GPU``:

  - Étude de différents papiers de recherche sur le sujet
  - Spécification d'un langage intermédiaire utilisable pour différents types
    d'accélérateurs (``GPU``, ``Cell``, ``Clearspeed``)
  - Développement du compilateur en ``C++`` à partir d'un existant
  - Reconnaissance de *pattern* de code ``C`` (boucle, réduction, ...)
  - Représentation du code ``C`` sous forme intermédiaire
  - Génération de code ``OpenGL`` pour cartes ``NVidia``
  - Génération de code spécialisé pour cartes ``ATI``
  - *packaging* du logiciel
  - Génération de pages ``HTML`` pour comparer les résultats de différents compilateurs en
    termes de performance et de justesse de calcul
  - Développement d'outils en ``python2/Qt4`` pour présenter le fonctionnement du compilateur
    sur différents codes afin de faire des démonstrations
  - Rédaction d’un manuel utilisateur (en français)

.. container:: skills

  Environnement UNIX, Compilation, C, C++, python2, HTML, OpenGL, Qt4, Subversion


Phillips
~~~~~~~~

.. container:: info

  2006
    Ingénieur Logiciel **Linux embarqué** dans le domaine de la **téléphonie mobile** |br|
    Mission de **4 mois** en tant que prestataire Yaccom |br|
    Régie déporté à **Rennes** et quelques jours au **Mans**

Développement de *drivers* ``linux`` pour une plateforme de type Smartphone ARM/PNX:

  - Mise en place de l'environnement de développement
  - Implémentation et test du *driver* ``LCD`` (*framebuffer*)
  - Implémentation et test du *driver* ``Caméra`` (*video4linux*)

.. container:: skills

  Environnement UNIX, C, python2, kernel linux, U-Boot, Subversion, électronique


Sagem
~~~~~

.. container:: info

  2005
    Ingénieur R&D dans le domaine de la **téléphonie mobile** |br|
    Mission de **2 mois** en tant que prestataire Yaccom |br|
    Forfait |br|
    poste basé à **Rennes**

Amélioration d’un outil de test écrit en C++ pour
envoyer et recevoir des commandes AT vers/depuis un *device* GSM:

  - *Design* du logiciel à partir du cahier des charges
  - Développement en C++ sous windows (``Visual C++``)
  - Intégration d'un interpréteur ``python2``  dans l'application
  - Affichage / Édition du *script* de test ``python2`` dans un  *widget* type ``TextEdit``
  - *Binding* des routines de transmission et de réception des Commandes AT vers le dispositif GSM
  - Surcharge des *callbacks* ``python2`` de *debugging* pour pouvoir exécuter le *script* de test
    en mode pas-a-pas
  - Redirection des fonctions standard de python2 ``print``, ``input()`` et ``raw_input()``
  - Élaboration d'un mode *batch* pour lancer plusieurs *scripts* à la suite et afficher
    l'état de chaque test

.. container:: skills

  Environnement Windows, C, C++, python2 (dont *internals*), Visual C++


Alcatel
~~~~~~~

.. container:: info

  2015
    Ingénieur R&D dans le domaine de la **téléphonie mobile** |br|
    Mission de **10 mois** pour Yaccom |br|
    Régie déporté à **Rennes**

Tâches effectuées:

  - Accompagnement de l'équipe lors de la migration de ``SunOS`` vers ``linux``
  - Étude de l'environnement de test existant
  - Passage occasionnel de tests et *bug reporting*
  - Correction de *bugs* dans le code du téléphone
  - Administration de différentes machines proposant des services sur le
    réseau de test (``DHCP``,  ``DNS``, ``SMTP``, ``POP3``, ``HTTP``, ``SyncML``,
    ``WAP``, ``NFS``)
  - Écriture d'un outil de test  en ``python2/Qt4`` pour tester différents
    protocoles de messagerie (``SMTP``, ``POP3``, messagerie ``i-mode``)
  - Rédaction du manuel utilisateur de l'outil (en anglais)
  - Formation sur l'outil (en anglais)

.. container:: skills

  Environnement UNIX, C, python2, perl, Qt4, DHCP, DNS, SMTP,
  POP3, HTTP, SyncML, WAP, NFS, xml/rpc, ClearCase


CAPS-Entreprise (Stage + CDD)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. container:: info

  2003-2004
    Ingénieur Logiciel dans le domaine de la **compilation** |br|
    Stage de **6 mois** + CDD **4 mois**

Création d'outils de démonstration pour présenter le savoir-faire de CAPS Entreprise
dans le domaine de l'optimisation de code.

Projet #1 (8 mois)
------------------

Développement d’un outil permettant d’appliquer automatiquement le modificateur
*restrict* du langage C sur les fichiers sources d’une application:

  - *Design* du logiciel en équipe
  - Développement  en ``C++``, ``python2`` et ``Qt4``
  - Mise en place d'un système de *profiling* de la mémoire
  - Analyse des données de *profiling*
  - Transformation *source-to-source*
  - IHM

.. container:: skills

  Environnement UNIX, C, C++, cvs, Qt4, gcc


Projet #2 (2 mois)
------------------

Développement d’un outil permettant de rechercher automatiquement
les meilleures options d'optimisation à appliquer au compilateur
à chacun des fichiers source  d’une application C.

  - *Design* du logiciel
  - Développement en  ``C++``, ``python2``, ``Qt4``
  - Système pour enregistrer et rejouer le processus de *build*
  - Mise en place de l'algorithme (génétique) de recherche de la "meilleure" combinaison
  - IHM

.. container:: skills

  Environnement UNIX, C, C++, python2, cvs, Qt4, gcc

|pagebreak|

Autres projets techniques
=========================

Otchado
~~~~~~~

.. container:: info

  Projet personnel démarré en **2021**

``otchado`` est un outil permettant de générer des images disque pour la plateforme RaspberryPi.
L'outil est basé sur les technologies ``docker``, ``qemu`` et ``python3``. Les images disque
générées sont dérivées de la distribution ``raspios``.

Voir https://gitlab.com/loghaan/otchado


.. container:: skills

  Environnement UNIX, python3, qemu, docker, RaspberryPi


Guitare/synthé
~~~~~~~~~~~~~~

.. container:: info

  Projet personnel démarré en **2017**


Réalisation d’un instrument de musique numérique (type guitare/synthé avec jeu en *tapping*).
"Bricolage" d'une veille guitare classique:

  - soudures sur les *frets* et les cordes métalliques pour obtenir une matrice 6x18
    (de contacts entre la corde et la *fret*)
  - Génération de paquets ``MIDI`` à l'aide d'une carte basée sur un ``AVR`` qui scrute la matrice
  - Réalisation d'une carte d'interface pour relier la liaison série de l'AVR à un *expander* ``MIDI``
  - Développement d'un synthétiseur basique pour ``RaspberryPi`` pour remplacer l |rsquo| *expander*

L'objectif à plus long terme est de réaliser un véritable instrument de musique qui soit agréable
à jouer.

.. container:: skills

  Environnement UNIX, C, C++, python, MIDI, kernel linux, AVR, git, SConstruct, ALSA,
  RaspberryPi, électronique


Concours de Robotique E=M6
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. container:: info

  1998-1999
    Projet réalisé en équipe pendant la 2ème année de DUT

Réalisation d'un robot autonome évoluant sur un terrain de la taille d'une table de
Ping-Pong:

  - Génération d'un signal ``PWM`` à partir d'un *bus* ``8 bits`` sur microcontrôleur ``8051``
    (en langage ``C``) 
  - Développement de la routine d'asservissement vitesse en ``C`` sur microcontrôleur ``8052``
  - Conception de cartes électroniques pour émettre un signal infrarouge (PCB)

.. container:: skills

  Environnement Windows, C, microcontrôleur 8051 & 8052, Protel, Autocad, électronique


A propos de ce document
~~~~~~~~~~~~~~~~~~~~~~~

Ce CV est écrit en ``restructuredText``. Le code source est disponible à l'adresse suivante: |br|
Voir https://gitlab.com/loghaan/loghaan_cv

.. container:: skills

  Environnement UNIX, sphinx, restrucutedText, HTML, CSS, weasyprint

