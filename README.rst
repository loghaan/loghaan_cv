.. code-block:: shell-session

    $ python3.11 -m venv --prompt loghaan_cv venv
    $ . venv/bin/activate
    $ pip install -r requirements.txt
    $ make
    $ evince sphinx/_build/loghaan_cv.pdf
