all:
	cd sphinx/_themes/default && sassc style.sass >../../_static/default.css
	cd sphinx && sphinx-build -v -b weasyprint . _build

clean:
	rm -fr sphinx/_build
	rm -f ../../_static/default.css
